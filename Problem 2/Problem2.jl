### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 2d935830-d84b-11eb-17fe-a98c384a732d
using Pkg

# ╔═╡ 0f75b24d-f6f5-4534-aa31-adf348237ca0
Pkg.add("SymPy")

# ╔═╡ e37c4904-1b03-4d7f-b0ed-7d9e4bcb8b75
using QuantEcon

# ╔═╡ 6b76cf82-fe47-47bd-89a1-dc91522df2e9
using PlutoUI

# ╔═╡ 21f1c9cc-a1cd-4af6-857b-779aac113017
using Printf

# ╔═╡ cd322c86-2332-4280-b368-9910adadf42f
using Games

# ╔═╡ 933b87cc-0c6a-4513-9561-1fddd66fc9c0
using SymPy

# ╔═╡ faf1d567-2064-4c29-bfa9-c948b0a9bd88
# needed to calculate probability 

# ╔═╡ 23561111-abf3-41c8-8528-0897c7a17d01
Pkg.instantiate()

# ╔═╡ 3465da84-0619-46e5-9d1e-db6be3a5ac29
md"## Creating the Normal Form Game"

# ╔═╡ 706b8312-6ea5-49ec-901c-ae775f99735d
begin
	Game_Matrix = NormalFormGame((2, 2)) # Creates the game matrix for sequential game
	Game_Matrix[1, 1] = [3,-3] # adds player utilities to row 1 col 1
	Game_Matrix[1, 2] = [-2, 2]  # adds player utilities to row 1 col 2
	Game_Matrix[2, 1] = [-1, 1]  # adds player utilities to row 2 col 1
	Game_Matrix[2, 2] = [0, 0]  # adds player utilities to row 2 col 2
	#Row values are for payer 1 and colvalues are for player 2
	#Game_Matrix[1, 1] = [a, b]  # adds player utilities to row 1 col 1
	#Game_Matrix[1, 2] = [c, d]  # adds player utilities to row 1 col 2
	#Game_Matrix[2, 1] = [e, f]  # adds player utilities to row 2 col 1
	#Game_Matrix[2, 2] = [g, h]  # adds player utilities to row 2 col 2
end

# ╔═╡ 78142f06-1c7d-4bdb-8941-2b3b0f4efdf7
# normal form games reduces the sequence of the game to a two by two matrix (2x2) of the players payoffs or utilities

# ╔═╡ 37e6cd54-73d8-445c-b9fe-baf9b0234578
# Checking the type of game_matrix
Game_Matrix

# ╔═╡ c407a5b0-451a-4e84-8e6c-fca9805baeb9
md"## Checkiing Player payoffs"

# ╔═╡ f264d987-46d4-4e6c-8d02-818675d1e4b0
Player_1 = Game_Matrix.players[1].payoff_array
	#= player 1 payoff matrix row
		a b   = r1,c1  r1,c2
		c d   = r2,c1  r2,c2
	=#

# ╔═╡ 16396a03-49b7-4442-a6e0-dc1449a65659
Player_2 = Game_Matrix.players[2].payoff_array
	#= player 2 payoff matrix col real value is from
		e f   = r1,c1  r2,c1
		g h   = r1,c2  r2,c2
	=#

# ╔═╡ 9ceb7233-8ca8-49dc-9fa7-ff360c28af85


# ╔═╡ 5dfd6da0-24a2-42d0-9bce-06cf236306ac
md"## Mixed Strategy"

# ╔═╡ 35e22a64-8dc4-45ac-af17-e6846353744a
function myMixStrat(game)
	p = Sym("p")
	q = Sym("q")
	# get player payoffs
	p1 = game.players[1].payoff_array
	#= player 1 payoff matrix row
		a b   = r1,c1  r1,c2
		c d   = r2,c1  r2,c2
	=#
	pl2 = game.players[2].payoff_array
	#= player 2 payoff matrix row
		e f   = r1,c1  r2,c1
		g h   = r1,c2  r2,c2
		
	to make it easy we will transpose the payoff matrix of player 2 
	=#
	p2 = transpose(pl2)
	
	# geting probability distribution. 
	
	# Mixstrat player 1
	cal_p = -1( p * pl2[2,1] + (1-p)*pl2[2,2]) + p * pl2[1,1] + (1-p)*pl2[1,2];
	ans_p = Eq(cal_p,0);
	mp = sympy.solve(ans_p,p)
	#this is a list
	p = convert(AbstractFloat, mp[1])
	p2 = 1-p
	
	# Mixstrat player 2
	cal_q = -1( q * p1[1,1] + (1-q)*p1[1,2]) + q * p1[2,1] + (1-q)*p1[2,2];
	ans_q = Eq(cal_q,0);
	mq = sympy.solve(ans_q,q)
	#this is a list
	q = convert(AbstractFloat, mq[1])	
	q2 = 1-q
	
	probabilities = [([p,p2],[q,q2])]
	
	return probabilities
	
end

# ╔═╡ 16638842-f712-42ee-94b9-0e4350c6c714


# ╔═╡ 8ff0328b-03ce-4bdc-9855-5d44322f8af7
# Mixed Strategy uses the following
# 1. Saddle points
# 2. Principal of Dominance
# 3. Nash Equilibrium

# ╔═╡ d42ecf18-0f86-484a-b3b6-57025e6e7c9b
md"### Saddle Points"

# ╔═╡ 6b16c707-b0d3-41d3-85f3-42e8ff624bf2
function saddlepoint(player)
	# WE KNOW THIS IS A 2X2 MATRIX SO WE CAN JUST USE NUMBERS
	a = player[1,1]
	b = player[1,2]
	c = player[2,1]
	d = player[2,2]
	
	mixmsg = "Game is a Mixed Strategy" # if no value is found
	msg = "Unfair Game" #if value of game is > 0
	m = "Game is a Pure Strategy"
	
	row1 = min(a,b)
	row2 = min(c,d)
	rowmax = max(row1, row2)
	
	col1 = max(a,c)
	col2 = max(b,d)
	
	colmin = min(col1,col2)
	
	if rowmax == colmin
		if rowmax > 0 #> rowmax
			return msg
		else
			return m
		end
	else
		return mixmsg
	end
end


# ╔═╡ eee2a28e-6073-4ae7-a190-1815d9478c10
saddlepoint(Player_1)

# ╔═╡ 2c741b7f-399c-4718-925f-d756b7d033ff
saddlepoint(Player_2)

# ╔═╡ edf8aef6-c68a-4049-95af-46d4e3e80c00
md"### Principal of Dominance"

# ╔═╡ 902635fe-37e4-475d-8330-39791a259dd4
# not needed in a 2x2 normal form game matrix game 

# ╔═╡ 891be950-9264-488d-b9bd-f43618da460d
md"### Nash Equilibrium"

# ╔═╡ e30794a3-afa1-4905-b6a3-362008575d35
probabilities = myMixStrat(Game_Matrix)

# ╔═╡ 9a73ab0b-e56f-498b-a7be-425ae1623982
function getStrategy(probs,player)
	# brake down the multi dimention matrix tuple
	P_probs = probs[1] #gets only the probabilities
	p = P_probs[player] # gets only the probabilities for the given player
	
	# geting the best strategy to play
	
	best = max(p[1],p[2])
	
	if best == p[1]
		row = 1
	else
		row = 2
	end
	
	return p, best, row
	#for player 2 row = col
end
	

# ╔═╡ 90edc2dc-7882-4022-936a-e929fcd3bc60
# player will chose row move

# ╔═╡ f9664c62-7ae7-4f14-9374-0ea438ca65c4
Player1_probs, Player1_best, Player1_move =getStrategy(probabilities,1)

# ╔═╡ ac4a8187-2382-4f51-a7a2-3bfacc80502a
# player will chose col move

# ╔═╡ dcbfb047-572b-4a57-8ff3-e5d204d8c6c6
Player2_probs, Player2_best, Player2_move =getStrategy(probabilities,2)

# ╔═╡ 5b00ea18-d3bb-4c20-a49d-45a546c1acbd
# this was my testers to create the function above

# ╔═╡ d524d4f0-babd-437d-be7c-185ba8af1948
#probs = pprobabilities[1]

# ╔═╡ b614181a-b07b-4208-bf05-4ca9397f87db
#P1_probs = probs[1]

# ╔═╡ 216feebc-a1c3-4940-bbcd-b86f92d7bf94
#p =P1_probs[1]

# ╔═╡ d8924b7f-a382-47e1-94f3-ce6c6868cc8b
#pb = p[1,1]

# ╔═╡ b4c631d2-0b56-40f0-9daa-9ed431d068e2


# ╔═╡ 0b385f49-a50b-4d64-9ce8-a405169c2750
md"### Solve Game"

# ╔═╡ 503120ce-f41a-4af3-8edf-00f633734f5d
function gameSolve(Player1, Player2, p1_move, p2_move)
	player1_action = Player1[p1_move, p2_move]
	player2_action = Player2[p1_move, p2_move]
	
	if player1_action > player2_action
		return "Player1 Wins"
	end
	if player1_action < player2_action
		return "Player2 Wins"
	end
	if player1_action == player2_action
		return "Game Drawn"
	end
end


# ╔═╡ 10c7a2aa-3337-47b5-bce1-668622088183
gameSolve(Player_1, Player_2 ,Player1_move, Player2_move)

# ╔═╡ Cell order:
# ╠═2d935830-d84b-11eb-17fe-a98c384a732d
# ╠═e37c4904-1b03-4d7f-b0ed-7d9e4bcb8b75
# ╠═6b76cf82-fe47-47bd-89a1-dc91522df2e9
# ╠═21f1c9cc-a1cd-4af6-857b-779aac113017
# ╠═cd322c86-2332-4280-b368-9910adadf42f
# ╠═0f75b24d-f6f5-4534-aa31-adf348237ca0
# ╠═faf1d567-2064-4c29-bfa9-c948b0a9bd88
# ╠═933b87cc-0c6a-4513-9561-1fddd66fc9c0
# ╠═23561111-abf3-41c8-8528-0897c7a17d01
# ╠═3465da84-0619-46e5-9d1e-db6be3a5ac29
# ╠═706b8312-6ea5-49ec-901c-ae775f99735d
# ╠═78142f06-1c7d-4bdb-8941-2b3b0f4efdf7
# ╠═37e6cd54-73d8-445c-b9fe-baf9b0234578
# ╠═c407a5b0-451a-4e84-8e6c-fca9805baeb9
# ╠═f264d987-46d4-4e6c-8d02-818675d1e4b0
# ╠═16396a03-49b7-4442-a6e0-dc1449a65659
# ╠═9ceb7233-8ca8-49dc-9fa7-ff360c28af85
# ╠═5dfd6da0-24a2-42d0-9bce-06cf236306ac
# ╠═35e22a64-8dc4-45ac-af17-e6846353744a
# ╠═16638842-f712-42ee-94b9-0e4350c6c714
# ╠═8ff0328b-03ce-4bdc-9855-5d44322f8af7
# ╠═d42ecf18-0f86-484a-b3b6-57025e6e7c9b
# ╠═6b16c707-b0d3-41d3-85f3-42e8ff624bf2
# ╠═eee2a28e-6073-4ae7-a190-1815d9478c10
# ╠═2c741b7f-399c-4718-925f-d756b7d033ff
# ╠═edf8aef6-c68a-4049-95af-46d4e3e80c00
# ╠═902635fe-37e4-475d-8330-39791a259dd4
# ╠═891be950-9264-488d-b9bd-f43618da460d
# ╠═e30794a3-afa1-4905-b6a3-362008575d35
# ╠═9a73ab0b-e56f-498b-a7be-425ae1623982
# ╠═90edc2dc-7882-4022-936a-e929fcd3bc60
# ╠═f9664c62-7ae7-4f14-9374-0ea438ca65c4
# ╠═ac4a8187-2382-4f51-a7a2-3bfacc80502a
# ╠═dcbfb047-572b-4a57-8ff3-e5d204d8c6c6
# ╠═5b00ea18-d3bb-4c20-a49d-45a546c1acbd
# ╠═d524d4f0-babd-437d-be7c-185ba8af1948
# ╠═b614181a-b07b-4208-bf05-4ca9397f87db
# ╠═216feebc-a1c3-4940-bbcd-b86f92d7bf94
# ╠═d8924b7f-a382-47e1-94f3-ce6c6868cc8b
# ╠═b4c631d2-0b56-40f0-9daa-9ed431d068e2
# ╠═0b385f49-a50b-4d64-9ce8-a405169c2750
# ╠═503120ce-f41a-4af3-8edf-00f633734f5d
# ╠═10c7a2aa-3337-47b5-bce1-668622088183
