### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 2d935830-d84b-11eb-17fe-a98c384a732d
using Pkg

# ╔═╡ e37c4904-1b03-4d7f-b0ed-7d9e4bcb8b75
using QuantEcon

# ╔═╡ 6b76cf82-fe47-47bd-89a1-dc91522df2e9
using PlutoUI

# ╔═╡ 21f1c9cc-a1cd-4af6-857b-779aac113017
using Printf

# ╔═╡ cd322c86-2332-4280-b368-9910adadf42f
using Games

# ╔═╡ 3465da84-0619-46e5-9d1e-db6be3a5ac29
md"## Creating the Normal Form Game"

# ╔═╡ 706b8312-6ea5-49ec-901c-ae775f99735d
begin
	Game_Matrix = NormalFormGame((2, 2)) # Creates the game matrix for sequential game
	Game_Matrix[1, 1] = [-8,-8] # adds player utilities to row 1 col 1
	Game_Matrix[1, 2] = [0, -10]  # adds player utilities to row 1 col 2
	Game_Matrix[2, 1] = [-10, 0]  # adds player utilities to row 2 col 1
	Game_Matrix[2, 2] = [-1, -1]  # adds player utilities to row 2 col 2
	#Row values are for payer 1 and colvalues are for player 2
	#Game_Matrix[1, 1] = [a, b]  # adds player utilities to row 1 col 1
	#Game_Matrix[1, 2] = [c, d]  # adds player utilities to row 1 col 2
	#Game_Matrix[2, 1] = [e, f]  # adds player utilities to row 2 col 1
	#Game_Matrix[2, 2] = [g, h]  # adds player utilities to row 2 col 2
end

# ╔═╡ 78142f06-1c7d-4bdb-8941-2b3b0f4efdf7
# normal form games reduces the sequence of the game to a two by two matrix (2x2) of the players payoffs or utilities

# ╔═╡ 37e6cd54-73d8-445c-b9fe-baf9b0234578
# Checking the type of game_matrix
Game_Matrix

# ╔═╡ c407a5b0-451a-4e84-8e6c-fca9805baeb9
md"## Checkiing Player payoffs"

# ╔═╡ f264d987-46d4-4e6c-8d02-818675d1e4b0
Player_1 = Game_Matrix.players[1].payoff_array

# ╔═╡ 6e7dad89-1f1d-4056-a0b9-23b97dc849bc


# ╔═╡ 16396a03-49b7-4442-a6e0-dc1449a65659
Player_2 = Game_Matrix.players[2].payoff_array

# ╔═╡ 5dfd6da0-24a2-42d0-9bce-06cf236306ac
md"## Mixed Strategy"

# ╔═╡ 8ff0328b-03ce-4bdc-9855-5d44322f8af7
# Mixed Strategy uses the following
# 1. Saddle points
# 2. Principal of Dominance
# 3. Nash Equilibrium

# ╔═╡ d42ecf18-0f86-484a-b3b6-57025e6e7c9b
md"### Saddle Points"

# ╔═╡ 6b16c707-b0d3-41d3-85f3-42e8ff624bf2
function saddlepoint(player)
	# WE KNOW THIS IS A 2X2 MATRIX SO WE CAN JUST USE NUMBERS
	a = player[1,1]
	b = player[1,2]
	c = player[2,1]
	d = player[2,2]
	
	mixmsg = "Game is a Mixed Strategy" # if no value is found
	msg = "Unfair Game" #if value of game is > 0
	m = "Game is a Pure Strategy"
	
	row1 = min(a,b)
	row2 = min(c,d)
	rowmax = max(row1, row2)
	
	col1 = max(a,c)
	col2 = max(b,d)
	
	colmin = min(col1,col2)
	
	if rowmax == colmin
		if rowmax > 0
			return msg
		else
			return m
		end
	else
		return mixmsg
	end
end


# ╔═╡ eee2a28e-6073-4ae7-a190-1815d9478c10
saddlepoint(Player_1)

# ╔═╡ 2c741b7f-399c-4718-925f-d756b7d033ff
saddlepoint(Player_2)

# ╔═╡ edf8aef6-c68a-4049-95af-46d4e3e80c00
md"### Principal of Dominance"

# ╔═╡ 902635fe-37e4-475d-8330-39791a259dd4
# not needed in a 2x2 normal form game matrix game 

# ╔═╡ be9d58c4-6260-41f1-8787-3adf22fed96c


# ╔═╡ 891be950-9264-488d-b9bd-f43618da460d
md"### Nash Equilibrium"

# ╔═╡ 20429fbb-0c44-42d1-aa4d-eead99a0cabf
pprobabilities = support_enumeration(Game_Matrix)

# ╔═╡ 9a73ab0b-e56f-498b-a7be-425ae1623982
function getStrategy(probs,player)
	# brake down the multi dimention matrix tuple
	P_probs = probs[1] #gets only the probabilities
	p = P_probs[player] # gets only the probabilities for the given player
	
	# geting the best strategy to play
	
	best = max(p[1],p[2])
	
	if best == p[1]
		row = 1
	else
		row = 2
	end
	
	return p, best, row
end
	

# ╔═╡ 90edc2dc-7882-4022-936a-e929fcd3bc60
# player will chose row move

# ╔═╡ f9664c62-7ae7-4f14-9374-0ea438ca65c4
Player1_probs, Player1_best, Player1_move =getStrategy(pprobabilities,1)

# ╔═╡ ac4a8187-2382-4f51-a7a2-3bfacc80502a
# player will chose col move

# ╔═╡ dcbfb047-572b-4a57-8ff3-e5d204d8c6c6
Player2_probs, Player2_best, Player2_move =getStrategy(pprobabilities,2)

# ╔═╡ 5b00ea18-d3bb-4c20-a49d-45a546c1acbd
# this was my testers to create the function above

# ╔═╡ d524d4f0-babd-437d-be7c-185ba8af1948
#probs = pprobabilities[1]

# ╔═╡ b614181a-b07b-4208-bf05-4ca9397f87db
#P1_probs = probs[1]

# ╔═╡ 216feebc-a1c3-4940-bbcd-b86f92d7bf94
#p =P1_probs[1]

# ╔═╡ d8924b7f-a382-47e1-94f3-ce6c6868cc8b
#pb = p[1,1]

# ╔═╡ b4c631d2-0b56-40f0-9daa-9ed431d068e2


# ╔═╡ 0b385f49-a50b-4d64-9ce8-a405169c2750
md"### Solve Game"

# ╔═╡ 503120ce-f41a-4af3-8edf-00f633734f5d
function gameSolve(Player1, Player2, p1_move, p2_move)
	player1_action = Player1[p1_move, p2_move]
	player2_action = Player2[p1_move, p2_move]
	
	if player1_action > player2_action
		return "Player1 Wins"
	end
	if player1_action < player2_action
		return "Player2 Wins"
	end
	if player1_action == player2_action
		return "Game Drawn"
	end
end


# ╔═╡ 10c7a2aa-3337-47b5-bce1-668622088183
gameSolve(Player_1, Player_2 ,Player1_move, Player2_move)

# ╔═╡ Cell order:
# ╠═2d935830-d84b-11eb-17fe-a98c384a732d
# ╠═e37c4904-1b03-4d7f-b0ed-7d9e4bcb8b75
# ╠═6b76cf82-fe47-47bd-89a1-dc91522df2e9
# ╠═21f1c9cc-a1cd-4af6-857b-779aac113017
# ╠═cd322c86-2332-4280-b368-9910adadf42f
# ╠═3465da84-0619-46e5-9d1e-db6be3a5ac29
# ╠═706b8312-6ea5-49ec-901c-ae775f99735d
# ╠═78142f06-1c7d-4bdb-8941-2b3b0f4efdf7
# ╠═37e6cd54-73d8-445c-b9fe-baf9b0234578
# ╠═c407a5b0-451a-4e84-8e6c-fca9805baeb9
# ╠═f264d987-46d4-4e6c-8d02-818675d1e4b0
# ╠═6e7dad89-1f1d-4056-a0b9-23b97dc849bc
# ╠═16396a03-49b7-4442-a6e0-dc1449a65659
# ╠═5dfd6da0-24a2-42d0-9bce-06cf236306ac
# ╠═8ff0328b-03ce-4bdc-9855-5d44322f8af7
# ╠═d42ecf18-0f86-484a-b3b6-57025e6e7c9b
# ╠═6b16c707-b0d3-41d3-85f3-42e8ff624bf2
# ╠═eee2a28e-6073-4ae7-a190-1815d9478c10
# ╠═2c741b7f-399c-4718-925f-d756b7d033ff
# ╠═edf8aef6-c68a-4049-95af-46d4e3e80c00
# ╠═902635fe-37e4-475d-8330-39791a259dd4
# ╠═be9d58c4-6260-41f1-8787-3adf22fed96c
# ╠═891be950-9264-488d-b9bd-f43618da460d
# ╠═20429fbb-0c44-42d1-aa4d-eead99a0cabf
# ╠═9a73ab0b-e56f-498b-a7be-425ae1623982
# ╠═90edc2dc-7882-4022-936a-e929fcd3bc60
# ╠═f9664c62-7ae7-4f14-9374-0ea438ca65c4
# ╠═ac4a8187-2382-4f51-a7a2-3bfacc80502a
# ╠═dcbfb047-572b-4a57-8ff3-e5d204d8c6c6
# ╠═5b00ea18-d3bb-4c20-a49d-45a546c1acbd
# ╠═d524d4f0-babd-437d-be7c-185ba8af1948
# ╠═b614181a-b07b-4208-bf05-4ca9397f87db
# ╠═216feebc-a1c3-4940-bbcd-b86f92d7bf94
# ╠═d8924b7f-a382-47e1-94f3-ce6c6868cc8b
# ╠═b4c631d2-0b56-40f0-9daa-9ed431d068e2
# ╠═0b385f49-a50b-4d64-9ce8-a405169c2750
# ╠═503120ce-f41a-4af3-8edf-00f633734f5d
# ╠═10c7a2aa-3337-47b5-bce1-668622088183
