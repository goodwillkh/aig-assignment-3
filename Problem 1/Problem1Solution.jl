### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 71742090-db73-11eb-2d4c-d9987e09ad7c
using Pkg

# ╔═╡ 7442f0de-b132-4930-a5e3-6379e1e99e0e
using PlutoUI

# ╔═╡ 1d5e4772-a69b-403c-a6e2-dcf0a0698a20
using Random

# ╔═╡ 1a8b93b4-a94c-4523-9808-1824c7abb7c9
md"## Creating the MDP struct and Functions "

# ╔═╡ 2fce2e70-6fb8-4118-81a2-290a3521b990
md"#### Abstruct MDP Class and MDP Structs"

# ╔═╡ 0fb49fbb-eb9d-447a-9da3-c0e443bf5c5d
RandomDeviceInstance = RandomDevice()

# ╔═╡ 3324da9f-6f2e-428d-8f67-20560869fb95
abstract type AbstractMarkovDecisionProcess end;

# ╔═╡ 053f782d-b24e-4bde-8e02-85217e5b1e0d
struct MarkovDecisionProcess{T} <: AbstractMarkovDecisionProcess
    initial::T
    states::Set{T}
    actions::Set{T}
    terminal_states::Set{T}
    transitions::Dict
    gamma::Float64
    reward::Dict

    function MarkovDecisionProcess{T}(initial::T, actions_list::Set{T}, terminal_states::Set{T}, transitions::Dict, states::Union{Nothing, Set{T}}, gamma::Float64) where T
        if (!(0 < gamma <= 1))
            error("MarkovDecisionProcess(): The gamma variable of an MDP must be between 0 and 1, the constructor was given ", gamma, "!");
        end
        local new_states::Set{typeof(initial)};
        if (typeof(states) <: Set)
            new_states = states;
        else
            new_states = Set{typeof(initial)}();
        end
        return new(initial, new_states, actions_list, terminal_states, transitions, gamma, Dict());
    end
end

# ╔═╡ 02663b22-f54d-49ad-bfa2-9f84b1576f4b
struct GridMarkovDecisionProcess <: AbstractMarkovDecisionProcess
    initial::Tuple{Int64, Int64}
    states::Set{Tuple{Int64, Int64}}
    actions::Set{Tuple{Int64, Int64}}
    terminal_states::Set{Tuple{Int64, Int64}}
    grid::Array{Union{Nothing, Float64}, 2}
    gamma::Float64
    reward::Dict

    function GridMarkovDecisionProcess(initial::Tuple{Int64, Int64}, terminal_states::Set{Tuple{Int64, Int64}}, grid::Array{Union{Nothing, Float64}, 2}; states::Union{Nothing, Set{Tuple{Int64, Int64}}}=nothing, gamma::Float64=0.999)
        if (!(0 < gamma <= 1))
            error("GridMarkovDecisionProcess(): The gamma variable of an MDP must be between 0 and 1, the constructor was given ", gamma, "!");
        end
        local new_states::Set{Tuple{Int64, Int64}};
        if (typeof(states) <: Set)
            new_states = states;
        else
            new_states = Set{Tuple{Int64, Int64}}();
        end
        local orientations::Set = Set{Tuple{Int64, Int64}}([(1, 0), (0, 1), (-1, 0), (0, -1)]);
        local reward::Dict = Dict();
        for i in 1:getindex(size(grid), 1)
            for j in 1:getindex(size(grid, 2))
                reward[(i, j)] = grid[i, j]
                if (!(grid[i, j] === nothing))
                    push!(new_states, (i, j));
                end
            end
        end
        return new(initial, new_states, orientations, terminal_states, grid, gamma, reward);
    end
end

# ╔═╡ 7de5bd84-2007-46ee-b965-39dbe146580f
md"## Reward function"

# ╔═╡ fdcb1a3a-49df-4898-a052-87017e2d0a45
function reward(mdp::T, state) where {T <: AbstractMarkovDecisionProcess}
    return mdp.reward[state];
end


# ╔═╡ 22928e6d-6412-463d-905e-a778ed0e2e6e
md"## Transition model"

# ╔═╡ 604bb4ad-4c39-4b78-8f8b-cfbd31c7f562
function transition_model(mdp::T, state, action) where {T <: AbstractMarkovDecisionProcess}
    if (length(mdp.transitions) == 0)
        error("transition_model(): The transition model for the given 'mdp' could not be found!");
    else
        return mdp.transitions[state][action];
    end
end

# ╔═╡ 0695cc8f-f303-4563-8ff0-09504316511a
function transition_model(gmdp::GridMarkovDecisionProcess, state::Tuple{Int64, Int64}, action::Nothing)
    return [(0.0, state)];
end

# ╔═╡ 086d7f5d-e6ef-47a0-8a78-12cf5293b2d4
md"## Actions"

# ╔═╡ dbcf7de8-4335-4785-b9bc-91532511a53b
function actions(mdp::T, state) where {T <: AbstractMarkovDecisionProcess}
    if (state in mdp.terminal_states)
        return Set{Nothing}([nothing]);
    else
        return mdp.actions;
    end
end

# ╔═╡ ab995386-4eeb-4bba-a086-30c1dd556ccf
md"## Next States Gerneration"

# ╔═╡ 7cb0cab9-9b66-4289-a028-c8659e93cf55
function go_to(gmdp::GridMarkovDecisionProcess, state::Tuple{Int64, Int64}, direction::Tuple{Int64, Int64})
    local next_state::Tuple{Int64, Int64} = map(+, state, direction);
    if (next_state in gmdp.states)
        return next_state;
    else
        return state;
    end
end

# ╔═╡ 4c960f3d-2105-4c2f-a824-7dccf8badad1
md"## Display the MDP Environment in Grid form"

# ╔═╡ 3e3573c2-0bb6-4d4a-bd5e-805cff1dd75d
function show_grid(gmdp::GridMarkovDecisionProcess, mapping::Dict)
    local grid::Array{Union{Nothing, Any}, 2};
    local rows::AbstractVector = [];
    for i in 1:getindex(size(gmdp.grid), 1)
        local row::Array{Union{Nothing, Any}, 1} = Array{Union{Nothing, Any}, 1}();
        for j in 1:getindex(size(gmdp.grid), 2)
            push!(row, get(mapping, (i, j), nothing));
        end
        push!(rows, reshape(row, (1, length(row))));
    end
    grid = reduce(vcat, rows);
    return grid;
end

# ╔═╡ e3c24d90-3b16-456a-8bb4-b51507953068
function to_arrows(gmdp::GridMarkovDecisionProcess, policy::Dict)
    local arrow_characters::Dict = Dict([Pair((0, 1), ">"),#= Right =#
                                        Pair((-1, 0), "^"),#= Up =#
                                        Pair((0, -1), "<"),#= Left =#
                                        Pair((1, 0), "v"),#= Down =#
                                        Pair(nothing, ".")]);#= obstacles and End =#
    return show_grid(gmdp, Dict(collect(Pair(state, arrow_characters[action])
                                    for (state, action) in policy)));
end

# ╔═╡ eef2d75b-ef1a-413d-8eb2-afdc2fdad656
md"# Defining the MDP in Grid form"

# ╔═╡ e5b2feae-e644-47e9-9206-c73efcb970f6
# The MDP is stored in the variable defined

# ╔═╡ 29a58214-2d6d-4e92-9064-d98c124e3f46
#= Defining a MDP in Grid form

1. First define an initial state as a co-ordinate (x,y) in the matrix defined 
(x,y) = (row,Col) where the agent will start
end in a comma

2. Define all Terminal states, this will be a vectors of co-ordinates ([(x,y),(x1,y1)...])
(x,y),(x1,y1) = (row,Col),(row1,Col1) where the agent will end the game

3. Map Rewards for the grid as a vector in matrix form each row ending in a ;
with negative infini numbers (-1) for lose states and Positive infini number (1) for win States. and negative numbers for noise states (0 >= x =>-1) or nothing to represent a wall

4. close all definitions into one set()
=#



# ╔═╡ 3b625891-ef4f-47be-8810-55a34d26722c
StoredMDP = GridMarkovDecisionProcess((1, 1),
                                            Set([(1, 2),(2, 4),(4, 4), (3, 4)]),
                                            [-0.04 -1 -0.04 -0.04 -0.04 -0.04;
                                            -0.04 nothing -0.04 -1 -0.04 -0.04;
                                            -0.04 -0.04 -0.04 +1 -0.04 -0.04;
		 									-0.04 nothing -0.04 -1 -0.04 -0.04])


# ╔═╡ d188c353-f386-43bf-83b2-3f1a68897184
md"# MDP POLICY FUNCTIONS"

# ╔═╡ 978cefd3-d320-4ba3-9c66-43d013286715
md"## Value Iteration"

# ╔═╡ 1862fa13-fe60-472a-8214-dbd44cf49449
md"## Expected Utility"

# ╔═╡ 3ea25ef4-87c7-4298-b038-27fc0491a001
md"## Policy Evaluation"

# ╔═╡ 565259da-97b7-4cd3-90d2-a5198fdb6582
md"## Policy Iteration"

# ╔═╡ 1dd144ab-dbab-4fc6-8fae-4b093f9b2a50
md"## Optimal Policy"

# ╔═╡ 2a38b1ca-0422-4a0e-b009-380e8de68475


# ╔═╡ d7e30b40-02d4-43ad-ba09-d5f5e12086da
md"## Index function Checks matrix for element"

# ╔═╡ 5b417f95-e58d-496c-b66e-d87025d63ecf
function index(v::Array{T, 1}, item::T) where {T <: Any}
    local i::Int64 = 0;
    for element in v
        i = i + 1;
        if (element == item)
            return i;
        end
    end
    return -1;          #couldn't find the item in the array
end

# ╔═╡ b1221fd9-0fd6-40b1-ba17-744aca060e75
function turn_heading(heading::Tuple{Any, Any}, inc::Int64)
    local o = [(1, 0), (0, 1), (-1, 0), (0, -1)];
    # 4 (for negative increments) - 1 (adjust index) = 3 offset
    return o[((index(o, heading) + inc + 3) % length(o)) + 1];
end

# ╔═╡ 729d85af-59d1-4217-b9b5-1b2020097237
function transition_model(gmdp::GridMarkovDecisionProcess, state::Tuple{Int64, Int64}, action::Tuple{Int64, Int64})
    return [(0.8, go_to(gmdp, state, action)),
            (0.1, go_to(gmdp, state, turn_heading(action, -1))),
            (0.1, go_to(gmdp, state, turn_heading(action, 1)))];
end

# ╔═╡ a7281ff9-3c04-4574-aa83-8e3cb37b8520
function value_iteration(gmdp::GridMarkovDecisionProcess; epsilon::Float64=0.001, maxiter::Int64=20)
    local U_prime::Dict = Dict(collect(Pair(state, 0.0) for state in gmdp.states));
    for iter in 1:maxiter
        local U::Dict = copy(U_prime);
        local delta::Float64 = 0.0
        for state in gmdp.states
            U_prime[state] = (reward(gmdp, state)
                            + (gmdp.gamma
                            * max((sum(collect(p * U[state_prime]
                                                for (p, state_prime) in transition_model(gmdp, state, action)))
                                        for action in actions(gmdp, state))...)));
            delta = max(delta, abs(U_prime[state] - U[state]));
        end
        println(delta)
        if (delta < ((epsilon * (1 - gmdp.gamma))/gmdp.gamma))
            return U
        end
    end
    return U_prime
end

# ╔═╡ a3cbf935-72d3-4b33-ac20-9465df45a245
function expected_utility(mdp::T, U::Dict, state::Tuple{Int64, Int64}, action::Tuple{Int64, Int64}) where {T <: AbstractMarkovDecisionProcess}
    return sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, state, action)));
end
#This has action as a tuple

# ╔═╡ bd87b6c1-9895-4cf6-a92f-42e95f94d339
function expected_utility(mdp::T, U::Dict, state::Tuple{Int64, Int64}, action::Nothing) where {T <: AbstractMarkovDecisionProcess}
    return sum((p * U[state_prime] for (p, state_prime) in transition_model(mdp, state, action)));
end
# No action is defined

# ╔═╡ 7c4bf11b-ea05-40b3-bc8c-37b57b80fafb
function policy_evaluation(pi::Dict, U::Dict, gmdp::GridMarkovDecisionProcess; k::Int64=200)
    for i in 1:k
        for state in gmdp.states
            U[state] = (reward(gmdp, state)
                        + (gmdp.gamma
                        * sum((p * U[state_prime] for (p, state_prime) in transition_model(gmdp, state, pi[state])))));
        end
    end
    return U;
end

# ╔═╡ 553fb217-0915-4216-b44e-ba26112a1389
md"## ARGMAX Gets best utility for a state"

# ╔═╡ af4d46cb-ea8d-4352-b2a4-96bfe37931fd
function argmax(seq::T, fn::Function) where {T <: Vector}
    local best_element = seq[1];
    local best_score = fn(best_element);
    for element in seq
        element_score = fn(element);
        if (element_score > best_score)
            best_element = element;
            best_score = element_score;
        end
    end
    return best_element;
end

# ╔═╡ 168d515f-8f1d-4fba-8157-7f48dcec9401
function policy_iteration(mdp::T) where {T <: AbstractMarkovDecisionProcess}
    local U::Dict = Dict(collect(Pair(state, 0.0) for state in mdp.states));
    local pi::Dict = Dict(collect(Pair(state, rand(RandomDeviceInstance, collect(actions(mdp, state))))
                                    for state in mdp.states));
    while (true)
        U = policy_evaluation(pi, U, mdp);
        local unchanged::Bool = true;
        for state in mdp.states
            local action = argmax(collect(actions(mdp, state)), (function(action::Union{Nothing, Tuple{Int64, Int64}})
                                                                    return expected_utility(mdp, U, state, action);
                                                                end));
            if (action != pi[state])
                pi[state] = action;
                unchanged = false;
            end
        end
        if (unchanged)
            println(show_grid(mdp, U))
            return pi;
        end
    end
end

# ╔═╡ 1a396e39-fe87-407b-b9f3-f92d84bf7ca4
function optimal_policy(mdp::T, U::Dict) where {T <: AbstractMarkovDecisionProcess}
    local pi::Dict = Dict();
    for state in mdp.states
        # @info "state:", state
        # @info " actions: ", collect(actions(mdp, state))
        # for a in (collect(actions(mdp, state)))
           # @info "utility of: ", a, " is ", expected_utility(mdp, U, state, a)
        # end

        pi[state] = argmax(collect(actions(mdp, state)), (function(action::Union{Nothing, Tuple{Int64, Int64}})
                                                                return expected_utility(mdp, U, state, action);
                                                            end));
        # @info "optimal: ", pi[state]
    end
    return pi;
end

# ╔═╡ 8740c90c-5ba8-44a3-b7ab-6bf085d53c00
md"# END OF Functions"

# ╔═╡ 2dc838a4-9a11-444d-a53d-e3bf1899ea6c


# ╔═╡ 20263665-dee9-49c8-9c39-d208e65cabe7
md"## Solving The Stored MDP"

# ╔═╡ b646c421-9bc7-4579-b43e-8a01f79f8ef5
md"## Returning expected utility at each Sate" 

# ╔═╡ d3859304-f623-4284-9a5e-067dcb030ca9
	U = value_iteration(StoredMDP, epsilon=0.0000001, maxiter=100)


# ╔═╡ 34722aa3-e5e7-47b2-8165-734ff64aaec0
r0 = show_grid(StoredMDP, U)

# ╔═╡ 9becaf76-ea93-4f72-bf7a-540c6d9c6fbd


# ╔═╡ a7bdf80e-77e4-4fdf-80ae-53919661677a
md"## For the optimal policy"

# ╔═╡ 3a667807-d203-4ec9-bc72-834aabab5435
fpi = optimal_policy(StoredMDP, U)

# ╔═╡ 3f8bf064-2f4c-4b5a-b242-1817fa4e0ec6
policy_evaluation(fpi,U,StoredMDP)

# ╔═╡ b1b6721a-caab-4036-a9ec-0649b7f33c7e
r2 = to_arrows(StoredMDP, fpi)

# ╔═╡ 90c5d3f7-f280-4abb-9c2e-f2e415ad5f70


# ╔═╡ 69c876b4-95f3-4e6c-94b7-d9a3d0a7e938
md"## Solving after policy iteration"

# ╔═╡ db7e43c5-6050-4c7a-9cd7-9ee1fe782ab7
pi = policy_iteration(StoredMDP)

# ╔═╡ 72875f0e-2b3c-4839-8192-d934c113e43f
r3 = to_arrows(StoredMDP, pi)

# ╔═╡ Cell order:
# ╠═71742090-db73-11eb-2d4c-d9987e09ad7c
# ╠═7442f0de-b132-4930-a5e3-6379e1e99e0e
# ╠═1d5e4772-a69b-403c-a6e2-dcf0a0698a20
# ╠═1a8b93b4-a94c-4523-9808-1824c7abb7c9
# ╠═2fce2e70-6fb8-4118-81a2-290a3521b990
# ╠═0fb49fbb-eb9d-447a-9da3-c0e443bf5c5d
# ╠═3324da9f-6f2e-428d-8f67-20560869fb95
# ╠═053f782d-b24e-4bde-8e02-85217e5b1e0d
# ╠═02663b22-f54d-49ad-bfa2-9f84b1576f4b
# ╠═7de5bd84-2007-46ee-b965-39dbe146580f
# ╠═fdcb1a3a-49df-4898-a052-87017e2d0a45
# ╠═22928e6d-6412-463d-905e-a778ed0e2e6e
# ╠═604bb4ad-4c39-4b78-8f8b-cfbd31c7f562
# ╠═0695cc8f-f303-4563-8ff0-09504316511a
# ╠═b1221fd9-0fd6-40b1-ba17-744aca060e75
# ╠═729d85af-59d1-4217-b9b5-1b2020097237
# ╠═086d7f5d-e6ef-47a0-8a78-12cf5293b2d4
# ╠═dbcf7de8-4335-4785-b9bc-91532511a53b
# ╠═ab995386-4eeb-4bba-a086-30c1dd556ccf
# ╠═7cb0cab9-9b66-4289-a028-c8659e93cf55
# ╠═4c960f3d-2105-4c2f-a824-7dccf8badad1
# ╠═3e3573c2-0bb6-4d4a-bd5e-805cff1dd75d
# ╠═e3c24d90-3b16-456a-8bb4-b51507953068
# ╠═eef2d75b-ef1a-413d-8eb2-afdc2fdad656
# ╠═e5b2feae-e644-47e9-9206-c73efcb970f6
# ╠═29a58214-2d6d-4e92-9064-d98c124e3f46
# ╠═3b625891-ef4f-47be-8810-55a34d26722c
# ╠═d188c353-f386-43bf-83b2-3f1a68897184
# ╠═978cefd3-d320-4ba3-9c66-43d013286715
# ╠═a7281ff9-3c04-4574-aa83-8e3cb37b8520
# ╠═1862fa13-fe60-472a-8214-dbd44cf49449
# ╠═a3cbf935-72d3-4b33-ac20-9465df45a245
# ╠═bd87b6c1-9895-4cf6-a92f-42e95f94d339
# ╠═3ea25ef4-87c7-4298-b038-27fc0491a001
# ╠═7c4bf11b-ea05-40b3-bc8c-37b57b80fafb
# ╠═565259da-97b7-4cd3-90d2-a5198fdb6582
# ╠═168d515f-8f1d-4fba-8157-7f48dcec9401
# ╠═1dd144ab-dbab-4fc6-8fae-4b093f9b2a50
# ╠═1a396e39-fe87-407b-b9f3-f92d84bf7ca4
# ╠═2a38b1ca-0422-4a0e-b009-380e8de68475
# ╠═d7e30b40-02d4-43ad-ba09-d5f5e12086da
# ╠═5b417f95-e58d-496c-b66e-d87025d63ecf
# ╠═553fb217-0915-4216-b44e-ba26112a1389
# ╠═af4d46cb-ea8d-4352-b2a4-96bfe37931fd
# ╠═8740c90c-5ba8-44a3-b7ab-6bf085d53c00
# ╠═2dc838a4-9a11-444d-a53d-e3bf1899ea6c
# ╠═20263665-dee9-49c8-9c39-d208e65cabe7
# ╠═b646c421-9bc7-4579-b43e-8a01f79f8ef5
# ╠═d3859304-f623-4284-9a5e-067dcb030ca9
# ╠═34722aa3-e5e7-47b2-8165-734ff64aaec0
# ╠═9becaf76-ea93-4f72-bf7a-540c6d9c6fbd
# ╠═a7bdf80e-77e4-4fdf-80ae-53919661677a
# ╠═3a667807-d203-4ec9-bc72-834aabab5435
# ╠═3f8bf064-2f4c-4b5a-b242-1817fa4e0ec6
# ╠═b1b6721a-caab-4036-a9ec-0649b7f33c7e
# ╠═90c5d3f7-f280-4abb-9c2e-f2e415ad5f70
# ╠═69c876b4-95f3-4e6c-94b7-d9a3d0a7e938
# ╠═db7e43c5-6050-4c7a-9cd7-9ee1fe782ab7
# ╠═72875f0e-2b3c-4839-8192-d934c113e43f
