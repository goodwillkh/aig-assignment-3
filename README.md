# AIG Assignment 3

Third Assignment
Due on 2/07/2021

Problem 1.
Mark: 25
Your task in this problem is to write a program in the Julia programming
language that performs the following:
• capture and store a Markov decision process (MDP) at runtime;
• given a policy, evaluate the policy and improve it to get a solution.

Key points
1. 

Problem 2.
Mark: 25
Write a program in the Julia programming language that captures a sequential
game between two players in its normal form and computes each
player’s payoff using a mixed strategy.

Key points.

1. Sequential Game
    Each player takes a turn to play
2. normal form
    makes use of Normal form games either a 2x2 matrix form with each player response recorded.
